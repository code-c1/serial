CC=gcc
CFLAGS=-Wall -Wextra -pedantic -std=c99 -pthread 

SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
EXEC=serial_test

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

doc:
	doxygen $(DOXYGEN_CONFIG_FILE)

clean:
	rm -f $(OBJS) $(EXEC)
