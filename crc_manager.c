/**
 * \file crc_manager.c
 *
 * \brief header file for crc manipulation
 * \author Gautier Levesque
 * \date 10-03-2024
 */

#include "crc_manager.h"

#include <stdio.h>
/**
 * \brief variable use to calculate CRC based on this polynomial
 */
static const uint16_t Polynomial = 0xA001;

/**
 * \brief array of corresponding for calculate CRC 16
 */
static uint16_t table_crc[256];
void init_crc_16(void)
{
    memset(table_crc, 0, sizeof(table_crc));
    uint16_t value;
    uint16_t temp;
    uint16_t table_crc_length = sizeof(table_crc) / 2;
    for (uint16_t i = 0; i < table_crc_length; ++i)
    {
        value = 0;
        temp = i;
        for (uint8_t j = 0; j < 8; ++j)
        {
            if (((value ^ temp) & 0x0001) != 0)
                value = (uint16_t)((value >> 1) ^ Polynomial);
            else
                value >>= 1;
            temp >>= 1;
        }

        table_crc[i] = value;
    }
}

uint16_t compute_checksum(const uint8_t *bytes, uint32_t size)
{
    return compute_checksum_from_seed(bytes, size, 0);
}

uint16_t compute_checksum_from_seed(const uint8_t *bytes, uint32_t size, uint16_t seed)
{
    uint16_t crc_tmp = seed;

    for (uint32_t i = 0; i < size; ++i)
    {
        uint8_t index = (uint8_t)(crc_tmp ^ bytes[i]);
        crc_tmp = (uint16_t)((crc_tmp >> 8) ^ table_crc[index]);
    }

    //printf("CRC computed : %u\r\n", crc_tmp);

    return crc_tmp;
}