/**
 * \file crc_manager.h
 * 
 * \brief header file for crc manipulation
 * \author Gautier Levesque
 * \date 10-03-2024
 */

#ifndef __CRC_MANAGER_H__
#define __CRC_MANAGER_H__

#include <stdint.h>
#include <string.h>

/**
 * \brief use to initialise table of correspond for crc purpose
 */
void init_crc_16(void);

/**
 * \brief wrapper call compute_checksum_from_seed with 0 in seed args
 *
 * \param bytes table of data to compute in crc
 * \param size size of bytes
 *
 * \return crc updated
 * 
 * \sa compute_checksum_from_seed
 */
uint16_t compute_checksum(const uint8_t *bytes, uint32_t size);

/**
 * \brief use to calculate crc depending of a previous value and a new data bytes
 *
 * \param bytes table of data to compute in crc
 * \param size size of bytes
 * \param seed previous value of crc (can be 0)
 * 
 * \return crc updated
 */
uint16_t compute_checksum_from_seed(const uint8_t *bytes, uint32_t size, uint16_t seed);

#endif