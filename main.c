/**
 * \brief main file contain all code implementation for serial port com found
 *
 * \file main.c
 * \author Gautier Levesque
 * \date 26-01-2024
 */

#include <stdio.h>
#include <dirent.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <termios.h>
#include <time.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sys/time.h>

#include "crc_manager.h"

/**
 * \brief define array size for com port
 */
#define MAX_COM_PORT 10

/**
 * \brief define size of com port name
 */
#define PORT_COM_NAME_SIZE 261

/**
 * \brief use to know how many max octet we can set in com port send before saturate memory
 */
#define MAX_OCTET_COM_PORT_SEND 255

/**
 * \brief use to know how many max octet we can set in com port receive before saturate memory
 */
#define MAX_OCTET_COM_PORT_RECEIVE 255

/**
 * \brief use to define type of message with motor
 */
typedef enum
{
    MSG_NONE = 0x00,
    MSG_VERIFY = 0x01,
    MSG_MOTOR_SPEED = 0x02,
    MSG_MOTOR_STOP = 0x03,
    MSG_MOTOR_PING = 0x05,
} Type_of_message;

/**
 * \brief use to instantiate a port com
 */
typedef struct
{
    char name[PORT_COM_NAME_SIZE];                   /**<name> name of com port*/
    bool is_open;                                    /**<is_open> if this com port is open*/
    bool recently_add;                               /**<recently_add> if we just add this*/
    int fd;                                          /**<fd> file descriptor when port is open*/
    uint8_t buf_receive[MAX_OCTET_COM_PORT_RECEIVE]; /**<buf_receive> buffer of received data from serial port*/
    uint8_t buf_send[MAX_OCTET_COM_PORT_SEND];       /**<uf_send> buffer of data need to be send to serial port*/
    uint8_t buf_receive_idx;                         /**<buf_receive_idx> count of octet present in buf_receive*/
    uint8_t buf_send_idx;                            /**<buf_send_idx> count of octet present in buf_send*/
    bool is_verify;                                  /**<is_verify> if port com is good*/
} Port_com;

/**
 * \brief simulate a timer
 */
typedef struct
{
    long long past_time;    /**<past_time> past time of activation of timer*/
    long long current_time; /**<current_time> current time of timer*/
    long long time_to_wait; /**<time_to_wait> time to wait before doing action*/
} Timer;

/**
 * \brief array of com port found
 */
typedef struct
{
    Port_com data[MAX_COM_PORT]; /**<data> array of com port*/
    uint8_t count;               /**<count> number of com port detected*/
} Port_com_array;

/**
 * \brief array of all com port found
 */
static Port_com_array port_com_array;

/**
 * \brief use to know if we found motor of raspberry
 */
static bool found_motor;

/**
 * \brief use to know when we need to ping
 */
static Timer timer_ping = {0, 0, 2000};

/**
 * \brief use to know when we need to resend a ping
 */
static Timer timer_before_resend_ping = {0, 0, 5000};

/**
 * \brief use to know when we need to read serial port
 */
static Timer timer_read = {0, 0, 50};

/**
 * \brief use to know when we need to write to serial port
 */
static Timer timer_write = {0, 0, 50};

/**
 * \brief motor speed left
 */
static int16_t motor_speed_left = 0;

/**
 * \brief motor speed right
 */
static int16_t motor_speed_right = -90;

/**
 * \brief use to parse a frame received from com port
 *
 * \param data data received
 * \param length how many octet are received
 * \param com_port_idx idx of com port
 */
void parse_frame(uint8_t *data, uint8_t length, uint8_t com_port_idx);

/**
 * \brief use to add a msg into port com buffer ready to send
 *
 * \param msg msg to send
 * \param msg_size size of msg
 * \param com pointer to current com port where we send message
 *
 * \return true if correctly inserted in buffer, false instead
 *
 */
bool add_msg_to_buffer(uint8_t *msg, uint8_t msg_size, Port_com *com);

/**
 * \brief use to set to true if a port com is verify
 *
 * \param idx idx in array of com port
 */
void port_com_is_verify(uint8_t idx);

/**
 * \brief use to scan all com port with a matching pattern
 *
 * \param pattern pattern to check
 *
 * \return true if success, false instead
 */
bool get_all_com_port(const char *pattern);

/**
 * \brief open a com port
 *
 * \param idx idx in array port com to open
 * \param baud_rate rate of com port
 *
 * \return true if success, false instead
 */
bool open_com_port(uint8_t idx, uint32_t baud_rate);

/**
 * \brief use to know if this port com concern motor or not
 *
 * \param idx index in array of com port already openned
 *
 * \return true if port is good, false instead
 */
bool verify_com_port(uint8_t idx);

/**
 * \brief function use to select port com connected with motor
 *
 * \param idx pointer to idx in array
 */
void select_motor(uint8_t *idx);

/**
 * \brief send ping to motor
 *
 * \param idx idx in array of com port
 */
void send_ping(uint8_t idx);

/**
 * \brief send motor stop
 *
 * \param idx idx in array of com port
 */
void send_motor_stop(uint8_t idx);

/**
 * \brief send a speed to motor
 *
 * \param idx idx in array of com port
 * \param speed_left speed left of motor
 * \param speed_right speed right of motor
 */
void send_motor_speed(uint8_t idx, int16_t speed_left, int16_t speed_right);

/**
 * \brief use to set speed receive by user in purcentage use by motor
 *
 * \param speed_x x receive value
 * \param speed_y y receive value
 */
void convert_speed_to_purcentage(int8_t speed_x, int8_t speed_y);

/**
 * \brief use to calculate millis time
 */
long long millis(void);

long long millis(void)
{
    struct timeval te;
    gettimeofday(&te, NULL);
    long long milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000LL;

    return milliseconds;
}

bool get_all_com_port(const char *pattern)
{
    DIR *dir;
    struct dirent *entry;

    printf("Getting all port com starting with %s\r\n", pattern);

    dir = opendir("/dev/");
    if (dir == NULL)
    {
        printf("Error when open /dev/ directory");
        return false;
    }

    uint8_t size_com_name = strlen(pattern);
    while ((entry = readdir(dir)) != NULL)
    {
        // see if file start with ACM
        if (strncmp(entry->d_name, pattern, size_com_name) == 0)
        {
            printf("Found : /dev/%s\r\n", entry->d_name);
            bool exist = false;
            bool idx_found = false;
            uint8_t idx_free = 0;
            // check if already exist in array
            for (uint8_t i = 0; i < MAX_COM_PORT; ++i)
            {
                if (strcmp(port_com_array.data[i].name, entry->d_name) == 0)
                {
                    exist = true;
                    break;
                }

                if (idx_found == false && port_com_array.data[i].recently_add == false && port_com_array.data[i].is_open == false)
                {
                    idx_found = true;
                    idx_free = i;
                }
            }

            if (exist == true || idx_found == false)
                continue;

            // adding in array
            memset(&port_com_array.data[idx_free], 0, sizeof(Port_com));
            port_com_array.data[idx_free].recently_add = true;
            sprintf(port_com_array.data[idx_free].name, "/dev/%s", entry->d_name);
            port_com_array.data[idx_free].fd = -1;
            port_com_array.count += 1;
        }
    }

    closedir(dir);

    return true;
}

bool open_com_port(uint8_t idx, uint32_t baud_rate)
{
    printf("Open port com : %s\r\n", port_com_array.data[idx].name);

    port_com_array.data[idx].fd = open(port_com_array.data[idx].name, O_RDWR | O_NONBLOCK);
    if (port_com_array.data[idx].fd < 0)
    {
        printf("Error when trying to open com port : %s\r\n", port_com_array.data[idx].name);
        port_com_array.data[idx].is_open = false;
        perror("Open failed");
        return false;
    }

    struct termios tty;

    memset(&tty, 0, sizeof(tty));
    if (tcgetattr(port_com_array.data[idx].fd, &tty) != 0)
    {
        perror("Error when getting serial port information");
        close(port_com_array.data[idx].fd);
        return false;
    }

    cfsetispeed(&tty, baud_rate);
    cfsetospeed(&tty, baud_rate);

    tty.c_cflag &= ~PARENB;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;

    // no flow control
    // tty.c_cflag &= ~CRTSCTS;

    tty.c_cflag |= CREAD | CLOCAL;
    tty.c_iflag &= ~(IXON | IXOFF | IXANY);

    tty.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    tty.c_oflag &= ~OPOST;

    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 0;
    // toptions.c_cc[VTIME] = 20;

    tcsetattr(port_com_array.data[idx].fd, TCSANOW, &tty);
    if (tcsetattr(port_com_array.data[idx].fd, TCSAFLUSH, &tty) != 0)
    {
        perror("Error when config serial port");
        close(port_com_array.data[idx].fd);
        return 1;
    }

    port_com_array.data[idx].is_open = true;
    perror("Open error : ");

    return true;
}

bool verify_com_port(uint8_t idx)
{
    if (idx > port_com_array.count)
    {
        printf("Wrong index to verify com port, too high\r\n");
        return false;
    }
    sleep(2);
    printf("Going to verify port com : %s\r\n", port_com_array.data[idx].name);

    uint8_t msg[6] = {0x01, MSG_VERIFY, 0x03, 0x01, 0x00, 0x00};
    uint8_t received_msg[20];
    memset(received_msg, 0, 20);

    uint16_t crc = compute_checksum(msg, 4);
    memcpy(msg + 4, &crc, sizeof(uint16_t));

    int bytes_written = 0;
    int bytes_read = 0;
    uint8_t max_attempt = 5;
    uint8_t current_attempt = 0;

    while (port_com_array.data[idx].is_verify == false && max_attempt > current_attempt++)
    {
        bytes_written = write(port_com_array.data[idx].fd, msg, sizeof(msg));
        printf("We send verify message\r\n");
        if (bytes_written == 6)
        {
            uint8_t max_time_before_timeout = 5;
            time_t start_time = time(NULL);

            while (port_com_array.data[idx].is_verify == false && time(NULL) - start_time < max_time_before_timeout)
            {
                bytes_read = read(port_com_array.data[idx].fd, received_msg, 20);
                if (bytes_read > 0)
                {
                    memcpy(port_com_array.data[idx].buf_receive + port_com_array.data[idx].buf_receive_idx, received_msg, bytes_read + port_com_array.data[idx].buf_receive_idx > 255 ? 0 : bytes_read);
                    port_com_array.data[idx].buf_receive_idx += bytes_read;
                    parse_frame(port_com_array.data[idx].buf_receive, port_com_array.data[idx].buf_receive_idx, idx);
                }
                else
                {
                    // printf("Byte read : %d\r\n", bytes_read);
                    // perror("read error");
                    sleep(1);
                }
            }
        }
        if (port_com_array.data[idx].is_verify == false)
        {
            printf("Attempt to verifiy port %s : %u/%u\r\n", port_com_array.data[idx].name, current_attempt, max_attempt);
            sleep(1);
        }
    }

    if (max_attempt < current_attempt)
    {
        printf("We can't verify port %s, cause we failed to many time to send message\r\n", port_com_array.data[idx].name);
        return false;
    }

    return port_com_array.data[idx].is_verify;
}

void parse_frame(uint8_t *data, uint8_t length, uint8_t com_port_idx)
{
    if (length == 0 || com_port_idx >= MAX_COM_PORT)
        return;

    bool start_of_message = false;
    uint8_t i = 0;

    for (; i < length; ++i)
    {
        if (data[i] == 0x01 && start_of_message == false)
        {
            start_of_message = true;
            break;
        }
        else if (start_of_message == false)
            continue;
    }

    uint8_t size_of_msg = 0;
    Type_of_message type_of_msg = MSG_NONE;
    if (length - 3 >= i)
    {
        memcpy(&size_of_msg, &data[i + 2], sizeof(uint8_t));
        memcpy(&type_of_msg, &data[i + 1], sizeof(uint8_t));
    }

    if (size_of_msg + i > length)
        return;

    uint16_t crc_receive = 0;
    memcpy(&crc_receive, data + i + 3 + size_of_msg - sizeof(uint16_t), sizeof(uint16_t));
    uint16_t crc_compute = compute_checksum(data + i, size_of_msg + 1);

    /*printf("CRC compute en hexa : %02X, et receive en hexa : %02X\r\n", crc_compute, crc_receive);
    printf("When parsing a full frame, crc : compute : %d et receive : %d\r\n", crc_compute, crc_receive);
    printf("frame receive : \r\n");
    for (uint8_t j = i; j < length; ++j)
    {
        printf("%02X,", data[j]);
    }
    printf("\r\n");*/

    if (crc_compute != crc_receive)
    {
        memset(port_com_array.data[com_port_idx].buf_receive, 0, 255);
        port_com_array.data[com_port_idx].buf_receive_idx = 0;
        return;
    }

    timer_ping.past_time = millis();

    switch (type_of_msg)
    {
    case MSG_VERIFY:
        if (data[i + 3] != 0x01)
        {
            break;
        }
        else
        {
            port_com_is_verify(com_port_idx);
            printf("Port confirmation receive\r\n");
        }
        break;
    case MSG_MOTOR_PING:
        if (data[i + 3] != 0x01)
        {
            send_ping(com_port_idx);
            printf("Ping bad receive\r\n");
        }
        else
        {
            printf("Ping receive\r\n");
        }
        break;
    case MSG_MOTOR_STOP:
        if (data[i + 3] != 0x01)
        {
            send_motor_stop(com_port_idx);
            printf("Stop bad receive\r\n");
        }
        else
        {
            printf("Stop receive\r\n");
        }
        break;
    case MSG_MOTOR_SPEED:
        if (data[i + 3] != 0x01)
        {
            send_motor_speed(com_port_idx, motor_speed_left, motor_speed_right);
            printf("Speed bad receive\r\n");
        }
        else
        {
            printf("Speed receive\r\n");
        }
        break;
    case MSG_NONE:
        break;
    default:
        printf("Message type unknow\r\n");
    }

    memset(port_com_array.data[com_port_idx].buf_receive, 0, 255);
    port_com_array.data[com_port_idx].buf_receive_idx = 0;
}

void port_com_is_verify(uint8_t idx)
{
    port_com_array.data[idx].is_verify = true;
}

void select_motor(uint8_t *idx)
{
    found_motor = false;
    port_com_array.count = 0;
    memset(port_com_array.data, 0, sizeof(Port_com) * MAX_COM_PORT);

    if (get_all_com_port("ttyACM") == false)
        printf("Unable to get all com PORT\r\n");

    for (uint8_t i = 0; i < port_com_array.count; ++i)
    {
        if (open_com_port(i, B115200) == false)
            continue;

        if (verify_com_port(i) == false)
        {
            port_com_array.data[i].recently_add = false;
            close(port_com_array.data[i].fd);
            printf("Port com : %s is bad\r\n", port_com_array.data[i].name);
        }
        else
        {
            *idx = i;
            found_motor = true;
            printf("Port com : %s is good\r\n", port_com_array.data[i].name);
        }
    }
}

void send_ping(uint8_t idx)
{
    // printf("We send a ping\r\n");
    uint8_t msg[6] = {0x01, MSG_MOTOR_PING, 0x03, 0x01, 0x00, 0x00};
    uint16_t crc = compute_checksum(msg, 4);
    memcpy(msg + 4, &crc, sizeof(uint16_t));

    add_msg_to_buffer(msg, sizeof(msg), &port_com_array.data[idx]);
}

void send_motor_stop(uint8_t idx)
{
    // printf("We send a ping\r\n");
    uint8_t msg[6] = {0x01, MSG_MOTOR_STOP, 0x03, 0x01, 0x00, 0x00};
    uint16_t crc = compute_checksum(msg, 4);
    memcpy(msg + 4, &crc, sizeof(uint16_t));

    add_msg_to_buffer(msg, sizeof(msg), &port_com_array.data[idx]);
}

void send_motor_speed(uint8_t idx, int16_t speed_left, int16_t speed_right)
{
    uint8_t msg[9] = {0x01, MSG_MOTOR_SPEED, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    memcpy(msg + 3, &speed_left, sizeof(int16_t));
    memcpy(msg + 5, &speed_right, sizeof(int16_t));
    uint16_t crc = compute_checksum(msg, 7);
    memcpy(msg + 7, &crc, sizeof(uint16_t));

    add_msg_to_buffer(msg, sizeof(msg), &port_com_array.data[idx]);
}

bool add_msg_to_buffer(uint8_t *msg, uint8_t msg_size, Port_com *com)
{
    if (com->buf_send_idx + msg_size > MAX_OCTET_COM_PORT_SEND)
        return false;

    memcpy(com->buf_send + com->buf_send_idx, msg, msg_size);
    com->buf_send_idx += msg_size;

    return true;
}

void convert_speed_to_purcentage(int8_t speed_x, int8_t speed_y)
{
    if (speed_y >= 1 && speed_x == 0)
    {
        motor_speed_left = 100;
        motor_speed_right = 100;
        return;
    }

    if (speed_y <= -1 && speed_x == 0)
    {
        motor_speed_left = -100;
        motor_speed_right = -100;
        return;
    }

    if (speed_x >= 1 && speed_y == 0)
    {
        motor_speed_left = 100;
        motor_speed_right = -100;
        return;
    }

    if (speed_x <= -1 && speed_y == 0)
    {
        motor_speed_left = -100;
        motor_speed_right = 100;
        return;
    }

    if (speed_x < 0)
    {
        if (speed_y < 0)
        {
            motor_speed_left = -50;
            motor_speed_right = 100;
        }
        else
        {
            motor_speed_left = 50;
            motor_speed_right = 100;
        }
    }
    else
    {
        if (speed_y < 0)
        {
            motor_speed_left = 100;
            motor_speed_right = -50;
        }
        else
        {
            motor_speed_left = 100;
            motor_speed_right = 50;
        }
    }
}

/**
 * \brief main function
 *
 * \return 0 if success or error code
 */
int main(void)
{
    uint8_t idx_port_com = 0;
    init_crc_16();
    port_com_array.count = 0;
    memset(port_com_array.data, 0, sizeof(Port_com) * MAX_COM_PORT);

    uint8_t received_msg[255];
    memset(received_msg, 0, 255);

    Timer timer_send_motor_speed = {0, 0, 5000};
    while (1)
    {
        if (found_motor == false)
        {
            select_motor(&idx_port_com);
            continue;
        }

        if(millis() - timer_send_motor_speed.past_time > timer_send_motor_speed.time_to_wait)
        {
            motor_speed_left *= -1;
            motor_speed_right *= -1;
            send_motor_speed(idx_port_com, motor_speed_left, motor_speed_right);
            timer_send_motor_speed.past_time = millis();
        }

        // ping com port task
        if (millis() - timer_ping.past_time > timer_ping.time_to_wait)
        {
            if (millis() - timer_before_resend_ping.past_time > timer_before_resend_ping.time_to_wait)
            {
                send_ping(idx_port_com);
                timer_before_resend_ping.past_time = millis();
            }
            timer_ping.past_time = millis();
        }

        // read com port task
        if (millis() - timer_read.past_time > timer_read.time_to_wait)
        {
            int16_t bytes_read = read(port_com_array.data[idx_port_com].fd, received_msg, 255);
            if (bytes_read > 0)
            {
                // printf("We read a message\r\n");
                memcpy(port_com_array.data[idx_port_com].buf_receive + port_com_array.data[idx_port_com].buf_receive_idx, received_msg, bytes_read + port_com_array.data[idx_port_com].buf_receive_idx > 255 ? 0 : bytes_read);
                memset(received_msg, 0, 255);
                port_com_array.data[idx_port_com].buf_receive_idx += bytes_read;
                parse_frame(port_com_array.data[idx_port_com].buf_receive, port_com_array.data[idx_port_com].buf_receive_idx, idx_port_com);
            }
            timer_read.past_time = millis();
        }

        // write com port task
        if (millis() - timer_write.past_time > timer_write.time_to_wait)
        {
            if (port_com_array.data[idx_port_com].buf_send_idx > 0)
            {
                // printf("We send a message\r\n");
                int bytes_written = write(port_com_array.data[idx_port_com].fd, port_com_array.data[idx_port_com].buf_send, port_com_array.data[idx_port_com].buf_send_idx);
                if (bytes_written > 0)
                {
                    uint8_t size_remaining = port_com_array.data[idx_port_com].buf_send_idx - bytes_written;
                    // printf("Byte to send : %u\r\n", size_remaining);
                    if (size_remaining > 0)
                        memcpy(port_com_array.data[idx_port_com].buf_send, port_com_array.data[idx_port_com].buf_send + bytes_written, size_remaining);
                    port_com_array.data[idx_port_com].buf_send_idx = size_remaining;
                }
            }
            timer_write.past_time = millis();
        }
    }

    for (uint8_t i = 0; i < port_com_array.count; ++i)
    {
        if (port_com_array.data[i].is_open == true)
        {
            close(port_com_array.data[i].fd);
            port_com_array.data[i].is_open = false;
            memset(port_com_array.data[i].name, '\0', PORT_COM_NAME_SIZE);
        }
    }

    return 0;
}